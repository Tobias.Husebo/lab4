package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class BriansBrain implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	int height;
	int width;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		height = rows;
		width = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);

		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				//Brians brain has dying aswell
				int random_color = random.nextInt(3);
				if (random_color == 0) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else if (random_color == 1) {
					currentGeneration.set(row, col, CellState.DEAD);
				} else {
					currentGeneration.set(row, col, CellState.DYING);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return height;
	}

	@Override
	public int numberOfColumns() {
		return width;
	}

	@Override
	public CellState getCellState(int row, int col) {
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		int max_rows = currentGeneration.numRows();
		int max_cols = currentGeneration.numColumns();
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int i=0; i < max_rows; i++) {
			for (int j=0; j < max_cols; j++) {
				CellState new_state = getNextCell(i, j);
				nextGeneration.set(i, j, new_state);
			}
		}
		currentGeneration=nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState dead_or_alive = currentGeneration.get(row, col);
		int neighbourState = countNeighbors(row, col, dead_or_alive);

		//Scenarioes for living cell:
		if (dead_or_alive == CellState.ALIVE){
			return CellState.DYING;
		}
		//Scenarioes for dying cell:
		else if (dead_or_alive == CellState.DYING) {
			return CellState.DEAD;
		} else {
			if (neighbourState == 2) {
				return CellState.ALIVE;
			} else {
				return CellState.DEAD;
			}
		}
		
	}

	private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;

		for (int i = row-1; i<row+2; i++) {
			for (int j = col-1; j<col+2; j++) {
				//checks incase of corner cells, then only += 0:
				if (i < 0 || i >= currentGeneration.numRows() || j < 0 || j >= currentGeneration.numColumns()) {
					counter += 0;
				//dosent count itself:
				} else if (i == row && j == col) {
					counter += 0;
				// if not corner cell, check if state equal to one iterating:
				} else {
					if (state == CellState.DEAD || state == CellState.DYING) {
						if ((currentGeneration.get(i, j) == CellState.ALIVE)){
							counter+=1;
						}
					}
					else if (state == CellState.ALIVE){
						if (currentGeneration.get(i, j) == state) {
							counter+=1;
						}
					}
				}
			}
		}

		return counter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
