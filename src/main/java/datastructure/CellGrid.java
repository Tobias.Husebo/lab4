package datastructure;

import java.util.ArrayList;
import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    ArrayList<CellState> grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new ArrayList<CellState>();

		for(int row=0; row<rows; row++) {
            for(int col=0; col<columns; col++) {
                grid.add(initialState);
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if((row >= 0 && row < rows) && (column >= 0 && column < columns)){ 
            //DEBUG: System.out.println("____\n" + row + "\n---" + column + "\n" + grid.size() + "\n index:" + ((rows*row)+(column*1)) + "\n|_________|\n");
            //Get index of said row & column:
            int indexOfElem = (columns*row) + (column*1); 

            //Remove old state and add new state:
            grid.set(indexOfElem, element);

        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        if((row >= 0 && row < rows) && (column >= 0 && column < columns)){ 
            //Get index of said row & column:
            int indexOfElem = (columns*row) + (column*1);

            //get index´s state: 
            CellState stateOfCurrentElem = grid.get(indexOfElem);
            return stateOfCurrentElem;

        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        CellGrid copiedGrid = new CellGrid(this.rows,this.columns,CellState.DEAD);
        
        //Iteration through copied grid, adding cellstates of prev grid:
        for(int row=0; row<this.rows; row++) {
            for(int col=0; col<this.columns; col++) {
                int indexOfElem = (columns*row) + (col*1);
                CellState cpElem = grid.get(indexOfElem); 
                copiedGrid.grid.add(indexOfElem, cpElem);
            }
        }
        return copiedGrid;
    }
    
}
